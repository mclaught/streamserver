/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ClientsWorker.h
 * Author: user
 *
 * Created on 28 октября 2020 г., 15:29
 */

#ifndef CLIENTSWORKER_H
#define CLIENTSWORKER_H

#include <list>
#include <mutex>
#include <atomic>

#include "MyThread.h"
#include "Client.h"

using namespace std;

class ClientsWorker : public MyThread {
public:
    ClientsWorker(int _index);
    ClientsWorker(const ClientsWorker& orig);
    virtual ~ClientsWorker();
    
//    void newClient(int sock);
//    void addClient(shared_ptr<Client> cli);
    int count();
    shared_ptr<Session> addSession(string sess);
    void removeSession(string sess);
    map<string, shared_ptr<Session>> getSessions();
    shared_ptr<Session> findSession(string session);
    
    int index;
private:
    map<string, shared_ptr<Session>> sessions;
    mutex sessionsMutex;
    
    virtual void exec();
};

#endif /* CLIENTSWORKER_H */

