/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RelxThread.cpp
 * Author: user
 * 
 * Created on 27 октября 2020 г., 17:47
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <iostream>
#include <map>
#include <set>
#include <mutex>
#include <vector>
//#include <sys/time.h>
#include <chrono>
#include <opus/opus.h>
#include <cstring>
#include <math.h>
#include <sched.h>
#include <atomic>
#include <fstream>

#include "RelxThread.h"
#include "Client.h"
#include "Session.h"
#include "config.h"

extern vector<ClientsWorker*> workers;

RelxThread::RelxThread() : MyThread() {
}

RelxThread::RelxThread(const RelxThread& orig) {
}

RelxThread::~RelxThread() {
}

void RelxThread::encode(string wav_name, string opus_name){
    
//    FILE* opus_f = fopen(opus_name.c_str(), "rb"); 
//    if(opus_f){
//        fclose(opus_f);
//        return;
//    }
    
    int err;
    OpusEncoder* encoder = opus_encoder_create(SAMPLE_RATE, CHANNELS, OPUS_APPLICATION_AUDIO, &err);
    if(err != OPUS_OK){
        cerr << "RELV OPUS: " << opus_strerror(err);
        return;
    }
    
    err = opus_encoder_ctl(encoder, OPUS_SET_VBR(0));
    if(err != OPUS_OK){
        cerr << "RELV OPUS: " << opus_strerror(err);
        encoder = nullptr;
        return;
    }
    
    err = opus_encoder_ctl(encoder, OPUS_SET_BITRATE(BITRATE));
    if(err != OPUS_OK){
        cerr << "RELV OPUS: " << opus_strerror(err);
        encoder = nullptr;
        return;
    }
    
    err = opus_encoder_ctl(encoder, OPUS_SET_SIGNAL(OPUS_SIGNAL_VOICE));
    if(err != OPUS_OK){
        cerr << "RELV OPUS: " << opus_strerror(err);
        encoder = nullptr;
        return;
    }
    
    FILE* f = fopen(wav_name.c_str(), "rb");
    if(!f){
        cerr << "Relax wav file error: " << strerror(errno) << endl;
        return;
    }
    
    FILE* opus_f = fopen(opus_name.c_str(), "wb");
    if(!opus_f){
        cerr << "Relax opus file error: " << strerror(errno) << endl;
        return;
    }
    
    fseek(f, 0, SEEK_END);
    int64_t f_sz = ftell(f);
    fseek(f, 0, SEEK_SET);
    int64_t rd_sz = 0;
    
    uint32_t samplerate; 
    uint16_t channels;
    int dataPos = readWavHeader(f, samplerate, channels);
    
    if(samplerate != SAMPLE_RATE || channels != CHANNELS){
        cerr << "Relax file: bad samplerate or channels" << endl;
        return;
    }
    
    opus_int16 frame[FRAME_SZ*CHANNELS];
    uint8_t opus[PACKET_SIZE];
    while(!feof(f)){
        memset(frame, 0, FRAME_SZ_BYTES);
        int rd = fread(reinterpret_cast<void*>(frame), sizeof(opus_int16), FRAME_SZ*CHANNELS, f);
        if(rd < 0){
            cerr << "File read error: " << strerror(errno) << endl;
            break;
        }else if(rd > 0){
            int opus_sz = opus_encode(encoder, frame, FRAME_SZ, opus, PACKET_SIZE);
            if(opus_sz < 0){
                cerr << "RELV opus_encode: " << opus_strerror(err);
                return;
            }
            fwrite(opus, 1, opus_sz, opus_f);
        }
        
        rd_sz += rd;
        cout << "Encode relax file...\t" << round(rd_sz*100/f_sz) << "%\r";
    }
    
    opus_encoder_destroy(encoder);
    fclose(f);
    
    cout << "Encode relax file...\tDone!!!\r";
}

void RelxThread::exec(){
    cpu_set_t mask;
    CPU_ZERO(&mask);
    CPU_SET(0, &mask);        
    if (sched_setaffinity(0, sizeof(cpu_set_t), &mask) == -1) {
        cerr << "Set affinity: " << strerror(errno) << endl;
    }
    
    FILE *f = fopen(RELX_OPUS_FILE, "rb");
    if(!f){
        cerr << "Relax opus file error: " << strerror(errno) << endl;
        return;
    }
    
    auto tm0 = chrono::steady_clock::now();
    
    uint8_t opus[PACKET_SIZE];
    while(!terminated){
        int rd = fread(opus, 1, PACKET_SIZE, f);
        if(rd < 0){
            cerr << "Opus file read: " << strerror(errno) << endl;
        }
        if(feof(f)){
            fseek(f, 0, SEEK_SET);
        }
        
        vector<shared_ptr<Session>> sessions_vector;
        {
            for(ClientsWorker* worker : workers){
                for(auto p : worker->getSessions()){
                    if(!p.second->hasGuide())
                        sessions_vector.push_back(p.second);
                }
            }
        }
        for(auto s : sessions_vector){
            s->mixin(opus, rd, CH_RELX);
        }
            
//        timespec tm;
//        clock_gettime(CLOCK_REALTIME, &tm);
//        int64_t dtm = ((int64_t)tm.tv_sec*1000000+(int64_t)tm.tv_nsec/1000) - ((int64_t)tm0.tv_sec*1000000+(int64_t)tm0.tv_nsec/1000);
        auto tm = chrono::steady_clock::now();
        int64_t dtm = chrono::duration_cast<chrono::microseconds>(tm - tm0).count();
        
        if(dtm > (FRAME_DUR_MS*1000-100))
            dtm = (FRAME_DUR_MS*1000-100);
//        cout << "RELX preiod = " << dtm << endl;
        
        usleep(FRAME_DUR_MS*1000 - dtm - 100);
        
//        clock_gettime(CLOCK_REALTIME, &tm0);
        tm0 = chrono::steady_clock::now();
    }
    
    cout << "Relx thread terminated" << endl;
}

int RelxThread::readWavHeader(FILE *f, uint32_t &samplerate, uint16_t &channels){
    const uint32_t RIFF = 'FFIR';
    const uint32_t WAVE = 'EVAW';
    const uint32_t DATA = 'atad';
    
    uint32_t riff;
    fread(&riff, 4, 1, f);
    if(riff != RIFF)
        return 0;
    
    uint32_t fLen;
    fread(&fLen, 4, 1, f);
    
    uint32_t wave;
    fread(&wave, 4, 1, f);
    if(wave != WAVE)
        return 0;
    
    uint32_t chId = 0;
    while(!feof(f)){
        fread(&chId, 4, 1, f);
        
        uint32_t chSz;
        fread(&chSz, 4, 1, f);
        
        if(chId == DATA)
            return ftell(f);
        else if(chId == ' tmf'){
            uint16_t format;
            fread(&format, 2, 1, f);
            
            fread(&channels, 2, 1, f);
            
            fread(&samplerate, 4, 1, f);
            
            uint32_t byterate;
            fread(&byterate, 4, 1, f);
            
            uint16_t blockAlign;
            fread(&blockAlign, 2, 1, f);
            
            uint16_t bits;
            fread(&bits, 2, 1, f);
            
            continue;
        }else{
            for(int i=0; i<chSz; i++){
                uint8_t n;
                fread(&n, 1, 1, f);
            }
        }
        
    }
    
    return 0;
}