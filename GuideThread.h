/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GuideThread.h
 * Author: user
 *
 * Created on 27 октября 2020 г., 17:04
 */

#ifndef GUIDETHREAD_H
#define GUIDETHREAD_H

#include "config.h"
#include "Session.h"
#include <opus/opus.h>

#define BUF_CNT 10

using namespace std;

class GuideThread {
public:
    GuideThread(int _sock, shared_ptr<Session> _session, string _type);
    GuideThread(const GuideThread& orig);
    virtual ~GuideThread();
    
    void add(int type, int sz, char* buf);
    void send(int type, int len, char* value);
private:
    int sock;
    shared_ptr<Session> session;
    OpusDecoder* decoder;
    mix_channels_t mix_ch;
    
    void sendToGuide(int type, int sz, char* data);
    void add_opus(int sz, char* buf);
    void add_state(int sz, char* buf);
    void add_photo(int sz, char* buf);
    void add_request(int sz, char* buf);
};

#endif /* GUIDETHREAD_H */

