/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Client.h
 * Author: user
 *
 * Created on 27 октября 2020 г., 14:59
 */

#ifndef CLIENT_H
#define CLIENT_H

#include <memory>
#include <functional>
#include <set>
#include <atomic>

#include "types.h"

using namespace std;

class GuideThread;
class Codec;
class Client;
class Session;

class Client {
public:
    Client(int _sock, string _type, string _session, string _format, shared_ptr<Session> _pSess);
    Client(const Client& orig);
    virtual ~Client();
    
    void free();
    
    static bool readString(int sock, string &str);
    static bool processHeader(int sock);
    static bool response(int sock, int code, string reason);
    void process_in();
    void dispose();
    
    int sock;
    int id;
    bool active = false;
    string session;
    string type;
    string format;
    GuideThread* thread;
    shared_ptr<Session> pSession;
private:
    bool is_free = true;
    char buffer[10000];
    char* buf_in = buffer;
    char* buf_out = buffer;
    
};

#endif /* CLIENT_H */

