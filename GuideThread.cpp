/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GuideThread.cpp
 * Author: user
 * 
 * Created on 27 октября 2020 г., 17:04
 */

#include <iostream>
#include <string.h>
#include <set>
#include <map>
#include <mutex>
#include <sys/types.h>
#include <sys/socket.h>
#include <atomic>

#include "GuideThread.h"
#include "Client.h"
#include "Session.h"
#include "types.h"
#include "json.hpp"

using namespace nlohmann;

extern set<shared_ptr<Client>> clients;

GuideThread::GuideThread(int _sock, shared_ptr<Session> _session, string _type) : sock(_sock), session(_session) {
    int err;
    decoder = opus_decoder_create(SAMPLE_RATE, 1, &err);
    if(err != OPUS_OK){
        cerr << "OPUS error: " << err << endl;
        return;
    }
    if(_type == "cliback")
        mix_ch = CH_CLIENT;
    else
        mix_ch = CH_GUIDE;
}

GuideThread::GuideThread(const GuideThread& orig) {
}

GuideThread::~GuideThread() {
}

void GuideThread::add(int type, int sz, char* buf){
    switch(type){
        case TYPE_OPUS:
            add_opus(sz, buf);
            break;
        case TYPE_STATE:
            add_state(sz, buf);
            break;
        case TYPE_PHOTO:
            add_photo(sz, buf);
            break;
    }
}

void GuideThread::add_opus(int sz, char* buf){

    if(session)
        session->mixin(reinterpret_cast<uint8_t*>(buf), sz, mix_ch);
    
//    opus_int16 pcm[FRAME_SZ];
//    int sample_cnt = opus_decode(decoder, reinterpret_cast<uint8_t*>(buf), sz, pcm, FRAME_SZ, 0);
//    if(sample_cnt < 0){
//        cerr << "Opus error: " << opus_strerror(sample_cnt) << endl;
//        return;
//    }
//
//    opus_int16 pcm2[FRAME_SZ*CHANNELS];
//    for(int i=0; i<sample_cnt; i++){
//        for(int j=0; j<CHANNELS; j++){
//            pcm2[i*CHANNELS+j] = pcm[i];
//        }
//    }
//    
//    if(session)
//        session->mixin(pcm2, mix_ch);
}

void GuideThread::sendToGuide(int type, int sz, char* data){
    auto s = atomic_load(&session);
    if(!s)
        return;
    auto g = s->getGuide();
    if(!g)
        return;
    
    if(g->thread){
        g->thread->send(type, sz, data);
    }
}

void GuideThread::add_state(int sz, char* buf){
    string str(buf, sz);
    try{
        json js = json::parse(str);
        if(js.is_object()){
            js["id"] = sock;
            
            str = js.dump(4, ' ');
            sendToGuide(TYPE_STATE, str.length(), const_cast<char*>(str.c_str()));
        }
    }catch(json::parse_error &e){
        cerr << "JSON: " << e.what() << endl;
    }
    
//    cout << "Location: " << sock << " - " << lat << ", " << lon << endl;
}

void GuideThread::add_photo(int sz, char* buf){
    string url(buf,sz);
    cout << "Photo: " << url << endl;
    
    sendToGuide(TYPE_PHOTO, sz, buf);
}

void GuideThread::add_request(int sz, char* buf){
    
}

void GuideThread::send(int type, int len, char* value){
    char* buf[len+3];
    tlv_t* tlv = reinterpret_cast<tlv_t*>(buf);
    tlv->type = type;
    tlv->len = len;
    memcpy(tlv->value, value, len);
    
    if(::send(sock, tlv, TLV_SZ(tlv), 0) == -1){
        cerr << "GuideThread.send: " << strerror(errno) << endl;
    }
}