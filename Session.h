/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Session.h
 * Author: user
 *
 * Created on 29 октября 2020 г., 21:14
 */

#ifndef SESSION_H
#define SESSION_H

#include <set>
#include <map>
#include <mutex>
#include <memory>
#include <opus/opus.h>

#include "config.h"
#include "types.h"
#include "Client.h"
#include "ClientsWorker.h"
//#include "Codec.h"
//#include "Client.h"

#define CODECS_CNT  4

using namespace std;

typedef int16_t buffer_t[FRAME_SZ*CHANNELS];

class Client;
class Codec;
class Session {
public:
//    Session();
    Session(string _session, ClientsWorker* _worker);
    Session(const Session& orig);
    Session(Session&& orig);
    virtual ~Session();
    
    static shared_ptr<Session> instance(string session);
    void dispose();
    
    string session;
    atomic<ClientsWorker*> worker;
    
    void addClient(shared_ptr<Client> cli, bool as_guide);
    void removeClient(shared_ptr<Client> cli);
    bool clearInactive();
    void getClients(list<shared_ptr<Client>> &lst);
    bool hasGuide();
    shared_ptr<Client> getGuide();
    bool isEmpty();
    void mixin(uint8_t* buf, int sz, mix_channels_t sender);
    void send(uint8_t* buf, int sz, int sender);
private:
    shared_ptr<Client> guide;
    mutex guideMutex;
    bool closed = true;
    Codec* codecs[CODECS_CNT];
    OpusDecoder* decoder;
};


#endif /* SESSION_H */

