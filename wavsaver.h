#ifndef WAVSAVER_H
#define WAVSAVER_H

#include <fstream>

using namespace std;

#pragma pack(push, 1)
typedef struct{
    uint16_t compression;
    uint16_t channles;
    uint32_t sample_rate;
    uint32_t bytes_per_sec;
    uint16_t allign;
    uint16_t bits_per_sample;
//    uint16_t extra;
//    uint8_t reserved[22];
}wav_format_t;
#pragma pack(pop)

class WavSaver
{
public:
    WavSaver();
    virtual ~WavSaver();
    
    int writeHeader(uint32_t sample_rate, int channels, char* buf);
private:
    int pos = 0;
    
    void add(char* buf, const char* data, int cnt);
};

#endif // WAVSAVER_H
