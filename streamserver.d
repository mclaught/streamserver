#!/bin/sh
### BEGIN INIT INFO
# Provides:          streamserver
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs $network
# Default-Start:     3 4 5
# Default-Stop:      0 1 6
# Short-Description: QuestGuide streaming server
# Description:       QuestGuide streaming server
### END INIT INFO

# Author: mclaught <sergey@taldykin.com>
#

DESC="QuestGuide streaming server"
NAME=streamserver
DAEMON=/usr/sbin/${NAME}

case $1 in
    start|restart|force-reload)
        echo "Starting ${DESC}..."

        cd /
        exec > /dev/null
        exec 2> /dev/null
        exec < /dev/null 

        pidof ${NAME}
        if [ $? -eq 0 ]
        then
            kill `pidof ${NAME}`
        fi

        ${DAEMON} &
        ;;

    stop)
        echo "Stoping ${DESC}..."

        pidof ${NAME}
        if [ $? -eq 0 ]
        then
            kill `pidof ${NAME}`
        fi
        ;;

    status)
        pidof ${NAME}
        if [ $? -eq 0 ]
        then
            echo "Running"
            exit 0
        else
            echo "Stoped"
            exit 1
        fi         
	;;
esac


