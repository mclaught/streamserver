/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on 27 октября 2020 г., 1:36
 */

#include <cstdlib>
#include <cstdio>
#include <faac.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <set>
#include <map>
#include <list>
#include <memory>
#include <mutex>
#include <vector>
#include <unistd.h>
#include <fcntl.h>
#include <syslog.h>
#include <signal.h>
#include <execinfo.h>
#include <netinet/tcp.h>

#include "config.h"
#include "Client.h"
#include "ClientsWorker.h"
#include "RelxThread.h"
#include "Session.h"

using namespace std;

vector<ClientsWorker*> workers;
RelxThread relxThread;
bool run = true;

int ConfigureSignalHandlers();

void print_usage(){
    cout << "Usage:" << endl;
    cout << "streamserver" << endl;
    cout << " - start server in terminal mode" << endl;
    cout << "streamserver -e <input wav file> <output opus file>" << endl;
    cout << " - encode WAV file to OPUS" << endl;
    cout << "streamserver -h" << endl;
    cout << " - this info" << endl;
}

/*
 * 
 */
int main(int argc, char** argv) {
    ConfigureSignalHandlers();
    
    if(argc > 1){
        string cmd(argv[1]);
        if(cmd == "-e"){
            if(argc == 4){
                relxThread.encode(string(argv[2]), string(argv[3]));
            }else{
                print_usage();
            }
        }else if(cmd == "-h"){
            print_usage();
        }
        exit(0);
    }
    
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1){
        cerr << "Error socket: " << strerror(errno) << endl;
        exit(1);
    }
    
    int arg = 1;
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &arg, sizeof(int)) == -1){
        cerr << "Error setsockopt: " << strerror(errno) << endl;
        exit(1);
    }
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &arg, sizeof(int)) == -1){
        cerr << "Error setsockopt: " << strerror(errno) << endl;
        exit(1);
    }
        
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(8085);
    if(bind(sock, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1){
        cerr << "Error bind: " << strerror(errno) << endl;
        exit(1);
    }
    
    int flags = fcntl(sock, F_GETFL);
    if(flags == -1){
        cerr << "fcntl: " << strerror(errno) << endl;
    }
    if(fcntl(sock, F_SETFL, flags | O_NONBLOCK) == -1){
        cerr << "fcntl: " << strerror(errno) << endl;
    }
    
    if(listen(sock, 100) == -1){
        cerr << "Error listen: " << strerror(errno) << endl;
        exit(1);
    }
    
    int worker_cnt = sysconf(_SC_NPROCESSORS_ONLN);
    cout << "Create " << worker_cnt << " workers" << endl;
    if(worker_cnt == 0)
        worker_cnt = 1;
    for(int i=0; i<worker_cnt; i++){
        ClientsWorker* worker = new ClientsWorker(i+1);
        workers.push_back(worker);
        worker->start();
    }
    
    relxThread.start();
    
    while(run){
        int max_sock = sock;
        
        fd_set rd_set;
        FD_ZERO(&rd_set);
        FD_SET(sock, &rd_set);
        
        max_sock++;
        
        timeval tv = {1,0};

        int cnt = select(max_sock, &rd_set, NULL, NULL, &tv);
        if(cnt == -1){
            cerr << "Error select: " << strerror(errno) << endl;
            continue;
        }else if(cnt > 0){
            if(FD_ISSET(sock, &rd_set)){
                sockaddr_in cli_addr;
                memset(&cli_addr, 0, sizeof(sockaddr_in));
                socklen_t addr_len = sizeof(sockaddr_in);
                
                int cli_sock = accept(sock, reinterpret_cast<sockaddr*>(&cli_addr), &addr_len);
                
                if(cli_sock == -1){
                    cerr << "Error accept: " << strerror(errno) << endl;
                    continue;
                }else{
                    int i = 1;
                    if(setsockopt(cli_sock, IPPROTO_TCP, TCP_NODELAY, &i, sizeof(int)) == -1){
                        cerr << "Error TCP_NODELAY: " << strerror(errno) << endl;
                    }
                    
                    if(!Client::processHeader(cli_sock)){
                        close(cli_sock);
                    }
                }
            }
        }
    }
    
    for(int i=0; i<worker_cnt; i++){
        workers[i]->terminate();
        delete workers[i];
    }
    relxThread.terminate();
    close(sock);
    
    
//    unsigned long samples_input, max_bytes_output;
//
//    faacEncHandle faacHandle = faacEncOpen(16000, 1, &samples_input, &max_bytes_output);
//    if(!faacHandle)
//        cerr << "FAAC not opened" << endl;
//    
//    faacEncConfigurationPtr faacCfg = faacEncGetCurrentConfiguration(faacHandle);
//    cout << "FAAC version: " << faacCfg->version << endl;
//    
////    faacCfg->aacObjectType = MAIN;
//    faacCfg->mpegVersion = MPEG4;    
//    faacCfg->useTns = 0;
//    faacCfg->allowMidside = 1;
//    faacCfg->bandWidth = 0;
//    faacCfg->outputFormat = 1;
//    faacCfg->inputFormat = FAAC_INPUT_16BIT;
//    faacCfg->bitRate = 64000;
//    
//    if (!faacEncSetConfiguration(faacHandle, faacCfg)){
//        cerr << "Bad config" << endl;
//    }
    
    return 0;
}

void log_error(string str)
{
    cerr << str << endl;
    syslog(LOG_DEBUG | LOG_LOCAL0, str.c_str());
}

static void signal_error(int sig, siginfo_t *si, void *ptr)
{
    void* ErrorAddr;
    void* Trace[16];
    int x;
    int TraceSize;
    char** Messages;
    
    // запишем в лог что за сигнал пришел


#if __WORDSIZE == 64 // если дело имеем с 64 битной ОС
    // получим адрес инструкции которая вызвала ошибку
    cerr << "Signal: " << strsignal(sig) << ", Addr: 0x" << hex << (uint64_t) si->si_addr << endl;
    syslog(LOG_LOCAL0 | LOG_ERR, "Signal: %s, Addr: 0x%lu\n", strsignal(sig), (uint64_t)si->si_addr);
    ErrorAddr = (void*) ((ucontext_t*) ptr)->uc_mcontext.gregs[REG_RIP];
#else 
    // получим адрес инструкции которая вызвала ошибку
    syslog(LOG_LOCAL0 | LOG_ERR, "Signal: %s, Addr: 0x%X\n", strsignal(sig), (int) si->si_addr);
    ErrorAddr = (void*) ((ucontext_t*) ptr)->uc_mcontext.gregs[REG_EIP];
#endif

    // произведем backtrace чтобы получить весь стек вызовов 
    TraceSize = backtrace(Trace, 16);
    Trace[1] = ErrorAddr;

    // получим расшифровку трасировки
    Messages = backtrace_symbols(Trace, TraceSize);
    if (Messages)
    {
        log_error("== Backtrace ==");

        // запишем в лог
        for (x = 1; x < TraceSize; x++)
        {
            log_error(Messages[x]);
        }

        log_error("== End Backtrace ==");
        free(Messages);
    }

    closelog();
    _exit(0);
}

void TermHandler(int sig)
{
    cerr << "caught TERM signal: " << strsignal(sig) << endl;
    run = false;
//    exit(0);
}

void HupHandler(int sig)
{
    cerr << "caught HUP signal: " << strsignal(sig) << endl;
    syslog(LOG_DEBUG | LOG_LOCAL0, "caught HUP signal: %s", strsignal(sig));
    run = false;
//    exit(0);
}

void IntHandler(int sig)
{
    cout << strsignal(sig) << endl;
    syslog(LOG_DEBUG | LOG_LOCAL0, "caught INT signal: %s", strsignal(sig));
    run = false;
//    exit(0);
}

int ConfigureSignalHandlers()
{
    int ign_sig[] = {SIGUSR2, SIGPIPE, SIGALRM, SIGTSTP, SIGPROF, SIGCHLD};
    int fatal_sig[] = {SIGQUIT, SIGILL, SIGTRAP, SIGABRT, SIGIOT, SIGBUS, SIGFPE, SIGSEGV, SIGSTKFLT, SIGCONT, SIGPWR, SIGSYS};

    for (int i = 0; i < 6; i++)
        signal(ign_sig[i], SIG_IGN);

    for (int i = 0; i < 12; i++)
    {
        struct sigaction sigact;
        sigact.sa_flags = SA_SIGINFO;
        sigact.sa_sigaction = signal_error;
        sigemptyset(&sigact.sa_mask);
        
        sigaction(fatal_sig[i], &sigact, 0);
    }

    struct sigaction sigtermSA;
    sigtermSA.sa_handler = TermHandler;
    sigemptyset(&sigtermSA.sa_mask);
    sigtermSA.sa_flags = 0;
    sigaction(SIGTERM, &sigtermSA, NULL);

    struct sigaction sigusr1SA;
    sigusr1SA.sa_handler = HupHandler;
    sigemptyset(&sigusr1SA.sa_mask);
    sigusr1SA.sa_flags = 0;
    sigaction(SIGUSR1, &sigusr1SA, NULL);

    struct sigaction sighupSA;
    sighupSA.sa_handler = HupHandler;
    sigemptyset(&sighupSA.sa_mask);
    sighupSA.sa_flags = 0;
    sigaction(SIGHUP, &sighupSA, NULL);

    struct sigaction sigintSA;
    sigintSA.sa_handler = IntHandler;
    sigemptyset(&sigintSA.sa_mask);
    sigintSA.sa_flags = 0;
    sigaction(SIGINT, &sigintSA, NULL);
    
    return 0;
}
