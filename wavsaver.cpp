#include "wavsaver.h"
#include <iostream>
#include <string.h>
#include <tuple>

WavSaver::WavSaver()
{
}

WavSaver::~WavSaver(){
}

void WavSaver::add(char* buf, const char* data, int cnt){
    memcpy(&buf[pos], data, cnt);
    pos += cnt;
}

int WavSaver::writeHeader(uint32_t sample_rate, int channels, char* buf)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wno-multichar"
    const uint32_t RIFF = (uint32_t)'FFIR';
    const uint32_t WAVE = (uint32_t)'EVAW';
    const uint32_t fmt = (uint32_t)' tmf';
    const uint32_t data_ = (uint32_t)'atad';
#pragma GCC diagnostic pop
    
    uint32_t size = 0;

    add(buf, reinterpret_cast<const char*>(&RIFF), 4);
    add(buf, reinterpret_cast<const char*>(&size), 4);
    add(buf, reinterpret_cast<const char*>(&WAVE), 4);

    wav_format_t format;
    format.compression = 1;
    format.channles = channels;
    format.sample_rate = sample_rate;
    format.bytes_per_sec = sample_rate*2*channels;
    format.allign = 2;
    format.bits_per_sample = 16;
//    format.extra = 0;
//    memset(format.reserved, 0, 22);
    size = sizeof(wav_format_t);
    add(buf, reinterpret_cast<const char*>(&fmt), 4);
    add(buf, reinterpret_cast<const char*>(&size), 4);
    add(buf, reinterpret_cast<const char*>(&format), sizeof(wav_format_t));

    size = 0;
    add(buf, reinterpret_cast<const char*>(&data_), 4);
    add(buf, reinterpret_cast<const char*>(&size), 4);
    
    return pos;
}
