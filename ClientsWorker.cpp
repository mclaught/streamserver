/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ClientsWorker.cpp
 * Author: user
 * 
 * Created on 28 октября 2020 г., 15:29
 */

#include <map>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <algorithm>
#include <sched.h>

#include "ClientsWorker.h"
#include "Session.h"
#include "Client.h"
#include "Session.h"


ClientsWorker::ClientsWorker(int _index) : index(_index) {
}

ClientsWorker::ClientsWorker(const ClientsWorker& orig) {
}

ClientsWorker::~ClientsWorker() {
}

void ClientsWorker::exec(){
    int proc_cnt = sysconf(_SC_NPROCESSORS_ONLN);
    int core_n = index % proc_cnt;
    cout << "Worker " << index << " CPU -> " << core_n << endl;
    
    cpu_set_t mask;
    CPU_ZERO(&mask);
    CPU_SET(core_n, &mask);        
    if (sched_setaffinity(0, sizeof(cpu_set_t), &mask) == -1) {
        cerr << "Worker Set affinity: " << strerror(errno) << endl;
    }
    
    while(!terminated){
        list<shared_ptr<Client>> clients_copy;
        {
            for(auto p : getSessions()){
                p.second->getClients(clients_copy);
            }
        }
        
        int max_sock = 0;
        fd_set rd_set, wr_set, er_set;
        FD_ZERO(&rd_set);
//        FD_ZERO(&wr_set);
        FD_ZERO(&er_set);
        
        for(shared_ptr<Client> cli : clients_copy){
            if(!cli->active)
                continue;

            FD_SET(cli->sock, &rd_set);
//            FD_SET(cli->sock, &wr_set);
            FD_SET(cli->sock, &er_set);
            
            if(cli->sock > max_sock)
                max_sock = cli->sock;
        }
        max_sock++;
        
        timeval tv = {1,0};
        
        int cnt = select(max_sock, &rd_set, NULL, &er_set, &tv);
        if(cnt == -1){
            cerr << "Error worker select: " << strerror(errno) << endl;
        }else if(cnt > 0){
            for(auto cli : clients_copy){
                if(FD_ISSET(cli->sock, &rd_set)){
                    cli->process_in();
                }
                if(FD_ISSET(cli->sock, &er_set)){
                    cli->dispose();
                }
//                if(FD_ISSET(cli->sock, &wr_set)){
//                    cout << "Ready write" << endl;
//                }
            }
        }
        
        for(auto p : getSessions()){
            if(p.second->clearInactive()){
                removeSession(p.first);
            }
        }
    }
    
    lock_guard<mutex> lock(sessionsMutex);
    sessions.clear();
    
    cout << "Worker " << index << " terminated" << endl;
}

//void ClientsWorker::newClient(int sock){
//    shared_ptr<Client> client = shared_ptr<Client>(Client::instance(sock));//new Client(sock)
//    if(!client){
//        close(sock);
//        return;
//    }
//    
//    client->on_session = [client, &sessions](const string& type, const string& session) -> shared_ptr<Session>{
//        shared_ptr<Session> sess;
//        {
//            map<string, shared_ptr<Session>>::iterator fnd_sess = sessions.end();
//            {
//                lock_guard<recursive_mutex> lock(sessionsMutex);
//                fnd_sess = sessions.find(session);
//            }
//
//            if(fnd_sess == sessions.end()){
//                cout << "Create session " << session << endl;
//                {
//                    lock_guard<recursive_mutex> lock(sessionsMutex);
//                    sessions.emplace(session, new Session(session));
//                }
//            }
//            
//            sess = sessions.find(session)->second;
//        }
//        
//        if(sess){
//            sess->addClient(client, type == "guide" || type == "cliback");
//        }
//        
//        client->on_session = nullptr;//[](const string& type, const string& session){};
//        
//        return sess;
//    };
//    client->on_close = [client, this](const string& session){
//        
//        auto fnd_sess = sessions.end();
//        {
//            lock_guard<recursive_mutex> lock(sessionsMutex);
//            fnd_sess = sessions.find(session);
//        }
//        if(fnd_sess != sessions.end()){
//            fnd_sess->second->removeClient(client);
//
//            if(fnd_sess->second->isEmpty()){// && fnd_sess->second->back==nullptr
//                {
//                    lock_guard<recursive_mutex> lock(sessionsMutex);
//                    sessions.erase(session);
//                }
//                cout << "Session " << session << " destroyed" << endl;
//            }
//        }
//        
//        client->on_close = nullptr;//[](const string& session){};
//    };
//
//    {
//        lock_guard<recursive_mutex> lock(clientsMutex);
//        
//        clients.push_back(client);
//        cout << "Add to worker " << index << ". Clients " << clients.size() << endl; 
//    }
//}

//void ClientsWorker::addClient(shared_ptr<Client> cli){
//    lock_guard<recursive_mutex> clock(clientsMutex);
//    clients.push_back(cli);
//}

shared_ptr<Session> ClientsWorker::addSession(string sess){
    lock_guard<mutex> lock(sessionsMutex);
    sessions.emplace(sess, new Session(sess, this));
    auto fnd = sessions.find(sess);
    if(fnd != sessions.end())
        return fnd->second;
    else
        return shared_ptr<Session>(nullptr);
}

void ClientsWorker::removeSession(string sess){
    lock_guard<mutex> lock(sessionsMutex);
    sessions.erase(sess);
}

map<string, shared_ptr<Session>> ClientsWorker::getSessions(){
    lock_guard<mutex> lock(sessionsMutex);
    return sessions;
}

shared_ptr<Session> ClientsWorker::findSession(string session){
    lock_guard<mutex> lock(sessionsMutex);
    auto fnd = sessions.find(session);
    if(fnd != sessions.end())
        return fnd->second;
    else
        return shared_ptr<Session>(nullptr);
}

int ClientsWorker::count(){
    lock_guard<mutex> lock(sessionsMutex);
    return sessions.size();
}