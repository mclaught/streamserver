/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyThread.h
 * Author: user
 *
 * Created on 16 января 2017 г., 9:56
 */

#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <thread>

using namespace std;

class MyThread {
private:
    static void _thread_func(MyThread* _this);
protected:
    thread _thrd;
public:
    bool terminated = false;
    
    MyThread();
    MyThread(const MyThread& orig);
    virtual ~MyThread();
    void start();
    virtual void terminate();
    
    virtual void exec() = 0;
};

#endif /* MYTHREAD_H */

