/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RelxThread.h
 * Author: user
 *
 * Created on 27 октября 2020 г., 17:47
 */

#ifndef RELXTHREAD_H
#define RELXTHREAD_H

#include <vector>

#include "MyThread.h"
#include "config.h"

class RelxThread : public MyThread {
public:
    RelxThread();
    RelxThread(const RelxThread& orig);
    virtual ~RelxThread();
    
    void encode(string wav_name, string opus_name);
private:
    virtual void exec();
    int readWavHeader(FILE *f, uint32_t &samplerate, uint16_t &channels);
};

#endif /* RELXTHREAD_H */

