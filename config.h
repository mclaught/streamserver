/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   config.h
 * Author: user
 *
 * Created on 28 октября 2020 г., 0:07
 */

#ifndef CONFIG_H
#define CONFIG_H

#define SAMPLE_RATE 48000
#define CHANNELS    2
#define FRAME_DUR_MS 20
#define FRAME_SZ (SAMPLE_RATE*FRAME_DUR_MS/1000)
#define FRAME_SZ_BYTES (FRAME_SZ * 2 * CHANNELS)

#define BITRATE 48000
#define PACKET_SIZE (BITRATE*FRAME_DUR_MS/8000)

#define RELX_WAV_FILE "/var/streamserver/relx48_2.wav"
#define RELX_OPUS_FILE "/var/streamserver/relx48_2.opus"

#endif /* CONFIG_H */

