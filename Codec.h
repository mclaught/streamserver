/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Codec.h
 * Author: user
 *
 * Created on 28 октября 2020 г., 13:43
 */

#ifndef CODEC_H
#define CODEC_H

#include <faac.h>
#include <opus/opus.h>
#include <string>
#include <mutex>
#include <list>

#include "types.h"
#include "Client.h"

using namespace std;

class Client;
class Codec {
public:
    Codec(int _sample_rate, int _channels, string _format);
    Codec(const Codec& orig);
    virtual ~Codec();
    
    virtual void init() = 0;
    virtual void sendHeader(shared_ptr<Client> cli);
    virtual void sendSock(int16_t* data, int sample_cnt, int sender) = 0;
    
    void addClient(shared_ptr<Client> cli);
    void removeClient(shared_ptr<Client> cli);
    bool clearInactive();
    void getClients(list<shared_ptr<Client>> &lst);
    bool isEmpty();
    
    string format;
protected:
    int sample_rate; 
    int channels;
    list<shared_ptr<Client>> clients;
    mutex clientsMutex;
    mutex codecMutex;
};

class WavSaver;
class WAVCodec : public Codec {
public:
    WAVCodec(int _sample_rate, int _channels);
    virtual ~WAVCodec();
    
    virtual void init();
    virtual void sendHeader(shared_ptr<Client> cli);
    virtual void sendSock(int16_t* data, int sample_cnt, int sender);
    
private:
    WavSaver* wav = nullptr;

};

class PCMCodec : public Codec {
public:
    PCMCodec(int _sample_rate, int _channels);
    virtual ~PCMCodec();
    
    virtual void init();
    virtual void sendSock(int16_t* data, int sample_cnt, int sender);
    
private:
    bool initialized = false;
};

class AACCodec : public Codec {
public:
    AACCodec(int _sample_rate, int _channels);
    virtual ~AACCodec();
    
    virtual void init();
    virtual void sendHeader(shared_ptr<Client> cli);
    virtual void sendSock(int16_t* data, int sample_cnt, int sender);
    
private:
    unsigned long samples_input, max_bytes_output;
    faacEncHandle faacHandle = nullptr;
    int16_t pcm[10240];
    int pcmSz = 0;
};

class OpusCodec : public Codec {
public:
    OpusCodec(int _sample_rate, int _channels);
    virtual ~OpusCodec();
    
    virtual void init();
    virtual void sendSock(int16_t* data, int sample_cnt, int sender);
    void sendOpus(uint8_t* buf, int sz, int sender);
    
private:
    OpusEncoder* encoder = nullptr;
    uint8_t opusBuf[1024];
};

#endif /* CODEC_H */

