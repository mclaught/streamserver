/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   types.h
 * Author: user
 *
 * Created on 2 ноября 2020 г., 13:52
 */

#ifndef TYPES_H
#define TYPES_H

#include <memory>

enum {
    TYPE_OPUS = 1,
    TYPE_STATE = 2,
    TYPE_SENDER = 3,
    TYPE_PHOTO = 4
};

typedef enum {
    CH_RELX,
    CH_GUIDE,
    CH_CLIENT
}mix_channels_t;

#pragma pack(push, 1)
typedef struct{
    uint8_t type;
    uint16_t len;
    char value[];
}tlv_t;
#define TLV_SZ(tlv) (tlv->len+3)

typedef struct{
    uint32_t id;
    double lat;
    double lon;
    uint8_t request;
}client_state_t;
#pragma pack(pop)

#endif /* TYPES_H */

