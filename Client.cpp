/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Client.cpp
 * Author: user
 * 
 * Created on 27 октября 2020 г., 14:59
 */

#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <vector>
#include <regex>
#include <functional>
#include <netinet/in.h>
#include <unistd.h>
#include <netinet/tcp.h>
#include <chrono>
#include <fcntl.h>

#include "Client.h"
#include "RelxThread.h"
#include "config.h"
#include "GuideThread.h"
#include "Codec.h"
#include "types.h"


Client::Client(int _sock, string _type, string _session, string _format, shared_ptr<Session> _pSess) 
    : sock(_sock), id(_sock), type(_type), session(_session), format(_format), pSession(_pSess) {
    
    thread = nullptr;
    if(_format == "opus"){
        thread = new GuideThread(_sock, _pSess, _type);
    }
    
    buf_in = buf_out = buffer;
    active = true;
    is_free = false;
        
    cout << "New client: " << id << " " << type << "-" << session << "-" << format  << endl;
}

Client::Client(const Client& orig) {
    cout << "Client copy constructor" << endl;
}

Client::~Client() {
    close(sock);
    if(thread){
        delete thread;
    }
    cout << "Client " << type << " id=" << id << " deleted" << endl;
}

void Client::free(){
    if(thread){
        delete thread;
        thread = nullptr;
    }
    
    cout << "Client " << type << " id=" << session << " destroyed" << endl;
    
    session = "";
    type = "";
    format = "";
    
    is_free = true;
}

void Client::dispose(){
    active = false;
}

vector<string> split(const string& input, const string& rgx) {
    regex re(rgx);
    sregex_token_iterator
        first{input.begin(), input.end(), re, -1},
        last;
    return {first, last};
}

bool Client::readString(int sock, string &str){
    auto tm0 = chrono::steady_clock::now();
    while(str.find("\r\n") == string::npos){
        char c;
        int rd = recv(sock, &c, 1, 0);
        if(rd < 0){
            cerr << "Client.readString: " << strerror(errno) << endl;
            return false;
        }else if(rd == 0){
            return false;
        }else{
            str += c;
        }
        
//        auto tm = chrono::steady_clock::now();
//        if(chrono::duration_cast<chrono::milliseconds>(tm - tm0).count() > 1000){
//            cerr << "Client.readString timeout" << endl;
//            return false;
//        }
    }
    return true;
}

bool Client::processHeader(int sock){
    string str;
    string type;
    string session;
    string format;
    while(str != "\r\n"){
        str.clear();
        if(readString(sock, str)){
            if(str.find("GET") == string::npos && str.find("POST") == string::npos)
                continue;
            
            vector<string> get_parts = split(str, " ");

    //            for(string str : get_parts){
    //                cout << str << endl;
    //            }

            if(get_parts.size() < 3){
                cerr << "HTTP header error" << endl;
                response(sock, 400, "Bad Request");
                return false;
            }

            vector<string> prms = split(get_parts[1], "/");

    //            for(string str : prms){
    //                cout << str << endl;
    //            }

            if(prms.size() < 4){
                cerr << "URL must have 3 parts" << endl;
                response(sock, 404, "Not Found");
                return false;
            }

            type    = prms[1];
            session = prms[2];
            format = prms[3];
        }else{
            return false;
        }
    }
    
//    if(!response(sock, 200, "OK")){
//        return false;
//    }
           
    if(!type.empty() && !session.empty() && !format.empty()){
        auto sess = Session::instance(session);
        if(sess){
            auto client = make_shared<Client>(sock, type, session, format, sess);
            sess->addClient(client, type == "guide" || type == "cliback");
            return true;
        }else{
            return false;
        }
    
    }else{
        return false;
    }
}

void Client::process_in(){
    char buf[1500];
    
    int rd = recv(sock, buf, 1500, 0);
    if(rd == -1){
        cerr << "Error client recv: " << strerror(errno) << endl;
        dispose();
        return;
    }
    if(rd == 0){
        dispose();
        return;
    }
    
    memcpy(buf_in, buf, rd);
    buf_in += rd;
    
    while(buf_out < buf_in){
        int buf_sz = buf_in-buf_out;

        if(thread){
            tlv_t* tlv = reinterpret_cast<tlv_t*>(buf_out);
            if(TLV_SZ(tlv)<=buf_sz){
                thread->add(tlv->type, tlv->len, tlv->value);
                buf_out += TLV_SZ(tlv);
            }else{
                break;
            }
        }else{
            buf_out = buf_in;
        }
    }
    
    if(buf_out == buf_in){
        buf_in = buf_out = buffer; 
    }else{
        int buf_sz = buf_in-buf_out;
        memmove(buffer, buf_out, buf_sz);
        buf_out = buffer;
        buf_in = buf_out+buf_sz;
    }
}

bool Client::response(int sock, int code, string reason){
    stringstream stm;
    stm << "HTTP/1.0 " << code << " " << reason << "\r\n";
    stm << "Content-type: application/octet-stream\r\n";
    stm << "Cache-Control: no-cache\r\n";
    stm << "Connection: close\r\n";
    stm << "\r\n";
    string resp = stm.str();
    
//    cout << resp;
    
    if(send(sock, resp.c_str(), resp.length(), 0) == -1){
        cerr << "Error client send: " << strerror(errno) << endl;
        return false;
    }
    return true;
}
