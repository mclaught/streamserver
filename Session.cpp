/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Session.cpp
 * Author: user
 * 
 * Created on 29 октября 2020 г., 21:14
 */

#include <string.h>
#include <iostream>
#include <sstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <vector>

#include "Client.h"
#include "Session.h"
#include "Codec.h"

extern vector<ClientsWorker*> workers;

//Session::Session() : session("") {
//}

Session::Session(string _session, ClientsWorker* _worker) : session(_session), guide(nullptr), worker(_worker) {
    codecs[0] = new WAVCodec(SAMPLE_RATE, CHANNELS);
    codecs[1] = new PCMCodec(SAMPLE_RATE, CHANNELS);
    codecs[2] = new AACCodec(SAMPLE_RATE, CHANNELS);
    codecs[3] = new OpusCodec(SAMPLE_RATE, CHANNELS);

    for(int j=0; j<CODECS_CNT; j++){
        codecs[j]->init();
    }
    
    int err;
    decoder = opus_decoder_create(SAMPLE_RATE, CHANNELS, &err);
    if(err != OPUS_OK){
        cerr << "Session opus decoder error: " << opus_strerror(err) << endl;
    }
}

Session::Session(const Session& orig) {
    session = orig.session;
    guide = orig.guide;
    memcpy(codecs, orig.codecs, sizeof(Codec*)*CODECS_CNT);
}

Session::Session(Session&& orig) {
    session = orig.session;
    guide = orig.guide;
    memcpy(codecs, orig.codecs, sizeof(Codec*)*CODECS_CNT);
    
    orig.session = "";
    orig.guide.reset();
    memcpy(orig.codecs, 0, sizeof(Codec*)*CODECS_CNT);
}

Session::~Session() {
    for(int i=0; i<CODECS_CNT; i++)
        if(codecs[i])
            delete codecs[i];
    
    if(decoder)
        opus_decoder_destroy(decoder);
    
    cout << "Session " << session << " deleted" << endl;
}

shared_ptr<Session> Session::instance(string session){
    shared_ptr<Session> sess;
    {
        {
            for(ClientsWorker* worker : workers){
                sess = worker->findSession(session);
                if(sess){
                    break;
                }
            }
        }

        if(!sess){
            cout << "Create session " << session << endl;
            {
                int _min = 1000000;
                ClientsWorker* _wrk = nullptr;
                for(ClientsWorker* worker : workers){
                    if(worker->count() < _min){
                        _min = worker->count();
                        _wrk = worker;
                    }
                }

                if(_wrk){
                    sess = _wrk->addSession(session);
                    cout << "Session -> worker " << _wrk->index << endl;
                }
            }
        }

    }
    return sess;
}

void Session::dispose(){
    closed = true;
    guide = nullptr;
    for(int i=0; i<CODECS_CNT; i++){
        if(codecs[i]){
            delete codecs[i];
            codecs[i] = nullptr;
        }
    }
}

void Session::addClient(shared_ptr<Client> cli, bool as_guide){
    shared_ptr<Client> to_dispose = nullptr;
        
    if(as_guide){
        {
            lock_guard<mutex> lock(guideMutex);
            to_dispose = guide;
            guide = cli;
        }
        
        stringstream stm;
        stm << "HTTP/1.0 200 OK\r\n";
        stm << "Content-type: application/octet-stream\r\n";
        stm << "Cache-Control: no-cache\r\n";
        stm << "Connection: close\r\n";
        stm << "\r\n";
        string resp = stm.str();

//        cout << resp;

        if(::send(cli->sock, resp.c_str(), resp.length(), 0) == -1){
            cerr << "Error session send: " << strerror(errno) << endl;
            cli->dispose();
        }
    }else{
        for(int i=0; i<CODECS_CNT; i++){
            if(cli->format == codecs[i]->format){
                codecs[i]->addClient(cli);
                break;
            }
        }
    }
        
    if(to_dispose)
        to_dispose->dispose();
}

void Session::removeClient(shared_ptr<Client> cli){
    {
        lock_guard<mutex> lock(guideMutex);
        if(guide == cli)
            guide.reset();
    }
    for(int i=0; i<CODECS_CNT; i++){
        codecs[i]->removeClient(cli);
    }
}

bool Session::clearInactive(){
    bool empty = true;
    {
        lock_guard<mutex> lock(guideMutex);
        if(guide){
            if(!guide->active)
                guide.reset();
            else
                empty = false;
        }
    }
    for(int i=0; i<CODECS_CNT; i++){
        if(!codecs[i]->clearInactive())
            empty = false;
    }
    return empty;
}

void Session::getClients(list<shared_ptr<Client>> &lst){
    {
        lock_guard<mutex> lock(guideMutex);
        if(guide)
            lst.push_back(guide);
    }
    for(int i=0; i<CODECS_CNT; i++){
        codecs[i]->getClients(lst);
    }
}

bool Session::hasGuide(){
    bool has = false;
    {
        lock_guard<mutex> lock(guideMutex);
        has = (bool)guide;//!=nullptr;
    }
    return has;
    
}

shared_ptr<Client> Session::getGuide(){
    shared_ptr<Client> g;
    {
        lock_guard<mutex> lock(guideMutex);
        g = guide;
    }
    return g;
}

bool Session::isEmpty(){
    {
        lock_guard<mutex> lock(guideMutex);
        if(guide)
            return false;
    }
    for(int i=0; i<CODECS_CNT; i++){
        if(!codecs[i]->isEmpty())
            return false;
    }
    return true;
}

void Session::mixin(uint8_t* buf, int sz, mix_channels_t sender){
    if(hasGuide()){
        if(sender==CH_GUIDE || sender==CH_CLIENT)
            send(buf, sz, 1);
        return;
    }
    if(sender == CH_RELX)
        send(buf, sz, 0);
}

void Session::send(uint8_t* buf, int sz, int sender){
    int dec_cnt = 0;
    opus_int16 frame[FRAME_SZ*CHANNELS];
    
    for(int i=0; i<CODECS_CNT; i++){
        if(!codecs[i]->isEmpty()){
            if(!dynamic_cast<OpusCodec*>(codecs[i])){
                if(dec_cnt == 0){
                    dec_cnt = opus_decode(decoder, buf, sz, frame, FRAME_SZ, 0);
                    if(dec_cnt < 0){
                        cerr << "Session opus decoder error: " << opus_strerror(dec_cnt) << endl;
                    }
                }
                if(dec_cnt > 0){
                    codecs[i]->sendSock(frame, dec_cnt, sender);
                }
            }else{//OPUS codec
                dynamic_cast<OpusCodec*>(codecs[i])->sendOpus(buf, sz, sender);
            }
        }
    }
}
