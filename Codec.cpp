/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Codec.cpp
 * Author: user
 * 
 * Created on 28 октября 2020 г., 13:43
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <iostream>
#include <string.h>
#include <netinet/in.h>
#include <sstream>
#include <set>

#include "Codec.h"
#include "config.h"
#include "wavsaver.h"
#include "types.h"
#include "Client.h"

Codec::Codec(int _sample_rate, int _channels, string _format) : 
    sample_rate(_sample_rate), channels(_channels), format(_format) {
}

Codec::Codec(const Codec& orig) {
}

Codec::~Codec() {
}

void Codec::addClient(shared_ptr<Client> cli){
    sendHeader(cli);
    
    lock_guard<mutex> lock(clientsMutex);
    clients.push_back(cli);
}

void Codec::removeClient(shared_ptr<Client> cli){
    lock_guard<mutex> lock(clientsMutex);
    clients.remove(cli);
}

bool Codec::clearInactive(){
    lock_guard<mutex> lock(clientsMutex);
    clients.remove_if([](shared_ptr<Client> cli) -> bool{
        return !cli->active;
    });
    return clients.empty();
}

void Codec::getClients(list<shared_ptr<Client>> &lst){
    lock_guard<mutex> lock(clientsMutex);
    for(auto cli : clients)
        lst.push_back(cli);
}

bool Codec::isEmpty(){
    lock_guard<mutex> lock(clientsMutex);
    return clients.empty();
}

void Codec::sendHeader(shared_ptr<Client> cli){
    stringstream stm;
    stm << "HTTP/1.0 200 OK\r\n";
    stm << "Content-type: application/octet-stream\r\n";
    stm << "Cache-Control: no-cache\r\n";
    stm << "Connection: close\r\n";
    stm << "\r\n";
    string resp = stm.str();
    
//    cout << resp;
    
    if(send(cli->sock, resp.c_str(), resp.length(), 0) == -1){
        cerr << "Error codec send: " << strerror(errno) << endl;
        cli->dispose();
    }
}

//========================= WAV ================================================
WAVCodec::WAVCodec(int _sample_rate, int _channels) : Codec(_sample_rate, _channels, "wav"){
}

WAVCodec::~WAVCodec(){
    if(wav)
        delete wav;
}

void WAVCodec::sendHeader(shared_ptr<Client> cli){
    WavSaver wav;

    char buf[1024];
    int cnt = wav.writeHeader(sample_rate, channels, buf);

    if(send(cli->sock, buf, cnt, 0) == -1){
        cerr << "Error codec send: " << strerror(errno) << endl;
        cli->dispose();
    }
    
    stringstream stm;
    stm << "HTTP/1.0 200 OK\r\n";
    stm << "Content-type: audio/wav\r\n";
    stm << "Cache-Control: no-cache\r\n";
    stm << "Connection: close\r\n";
    stm << "\r\n";
    string resp = stm.str();
    
//    cout << resp;
    
    if(send(cli->sock, resp.c_str(), resp.length(), 0) == -1){
        cerr << "Error codec send: " << strerror(errno) << endl;
        cli->dispose();
    }
}

void WAVCodec::init(){
    if(wav)
        return;
}

void WAVCodec::sendSock(int16_t* data, int sample_cnt, int sender){
    
    list<shared_ptr<Client>> tmp_clients;
    getClients(tmp_clients);
    for(shared_ptr<Client> cli : tmp_clients){
        if(!cli->active)
            continue;
        if(send(cli->sock, data, sample_cnt*sizeof(int16_t)*channels, 0) == -1){
            cerr << "Error codec send: " << strerror(errno) << endl;
            cli->dispose();
        }
    }
}

//========================= PCM ================================================
PCMCodec::PCMCodec(int _sample_rate, int _channels) : Codec(_sample_rate, _channels, "pcm"){
}

PCMCodec::~PCMCodec(){
}

void PCMCodec::init(){
    if(!initialized){
        initialized = true;
    }
}

void PCMCodec::sendSock(int16_t* data, int sample_cnt, int sender){
    list<shared_ptr<Client>> tmp_clients;
    getClients(tmp_clients);
    for(shared_ptr<Client> cli : tmp_clients){
        if(!cli->active)
            continue;
        if(send(cli->sock, data, sample_cnt*sizeof(int16_t)*channels, 0) == -1){
            cerr << "Error codec send: " << strerror(errno) << endl;
            cli->dispose();
        }
    }
}

//========================= AAC ================================================
AACCodec::AACCodec(int _sample_rate, int _channels) : Codec(_sample_rate, _channels, "aac"){
}

AACCodec::~AACCodec(){
    if(faacHandle)
        faacEncClose(faacHandle);
}

void AACCodec::sendHeader(shared_ptr<Client> cli){
    stringstream stm;
    stm << "HTTP/1.0 200 OK\r\n";
    stm << "Content-type: audio/aac\r\n";
    stm << "Cache-Control: no-cache\r\n";
    stm << "Connection: close\r\n";
    stm << "\r\n";
    string resp = stm.str();
    
//    cout << resp;
    
    if(send(cli->sock, resp.c_str(), resp.length(), 0) == -1){
        cerr << "Error codec send: " << strerror(errno) << endl;
        cli->dispose();
    }
}

void AACCodec::init(){
    if(faacHandle)
        return;

    faacHandle = faacEncOpen(sample_rate, channels, &samples_input, &max_bytes_output);
    if(!faacHandle)
        cerr << "FAAC not opened" << endl;

    faacEncConfigurationPtr faacCfg = faacEncGetCurrentConfiguration(faacHandle);
//    cout << "FAAC version: " << faacCfg->version << endl;

//    faacCfg->aacObjectType = MAIN;
    faacCfg->mpegVersion = MPEG4;    
    faacCfg->useTns = 0;
    faacCfg->allowMidside = 1;
    faacCfg->bandWidth = 0;
    faacCfg->outputFormat = 1;
    faacCfg->inputFormat = FAAC_INPUT_16BIT;
    faacCfg->bitRate = 64000;

    if (!faacEncSetConfiguration(faacHandle, faacCfg)){
        cerr << "Bad config" << endl;
    }
}

void AACCodec::sendSock(int16_t* data, int sample_cnt, int sender){
    if(!faacHandle)
        return;
    
    memcpy(&pcm[pcmSz], data, sample_cnt*sizeof(int16_t)*channels);
    pcmSz += sample_cnt*channels;

    if(pcmSz >= samples_input){

        unsigned char aac[max_bytes_output];
        int aacSz = 0;
        {
            lock_guard<mutex> lock(codecMutex);
            aacSz = faacEncEncode(faacHandle, reinterpret_cast<int32_t*>(pcm), samples_input, aac, max_bytes_output);
        }

        if(aacSz > 0){
            list<shared_ptr<Client>> tmp_clients;
            getClients(tmp_clients);
            for(shared_ptr<Client> cli : tmp_clients){
                if(!cli->active)
                    continue;
                if(send(cli->sock, aac, aacSz, 0) == -1){
                    cerr << "Error codec send: " << strerror(errno) << endl;
                    cli->dispose();
                }
            }
        }

        memmove(pcm, &pcm[samples_input], (pcmSz-samples_input)*sizeof(int16_t));
        pcmSz -= samples_input;
    }
}

//========================= Opus ================================================
OpusCodec::OpusCodec(int _sample_rate, int _channels) : Codec(_sample_rate, _channels, "opus"){
}

OpusCodec::~OpusCodec(){
    if(encoder){
        opus_encoder_destroy(encoder);
        encoder = nullptr;
    }
}

void OpusCodec::init(){
    if(encoder)
        return;
    
    int err;
    encoder = opus_encoder_create(sample_rate, channels, OPUS_APPLICATION_VOIP, &err);
    if(err != OPUS_OK){
        cerr << "Opus encoder error: " << opus_strerror(err);
        encoder = nullptr;
        return;
    }
    
    err = opus_encoder_ctl(encoder, OPUS_SET_VBR(0));
    if(err != OPUS_OK){
        cerr << "Opus encoder error: " << opus_strerror(err);
        encoder = nullptr;
        return;
    }
    
    err = opus_encoder_ctl(encoder, OPUS_SET_BITRATE(BITRATE));
    if(err != OPUS_OK){
        cerr << "Opus encoder error: " << opus_strerror(err);
        encoder = nullptr;
        return;
    }
    
    err = opus_encoder_ctl(encoder, OPUS_SET_SIGNAL(OPUS_SIGNAL_VOICE));
    if(err != OPUS_OK){
        cerr << "Opus encoder error: " << opus_strerror(err);
        encoder = nullptr;
        return;
    }
}

void OpusCodec::sendSock(int16_t* data, int sample_cnt, int sender){
    if(!encoder)
        return;
    
    for(int16_t* p = data; p < (data+sample_cnt*channels); p += FRAME_SZ*channels){
        uint8_t opus[1024];
        int opus_sz = 0;
        {
            lock_guard<mutex> lock(codecMutex);
            opus_sz = opus_encode(encoder, p, FRAME_SZ, opus, 1024);
        }
        
        if(opus_sz < 0){
            cerr << "Opus encoder error: " << opus_strerror(opus_sz);
        }else if(opus_sz > 0){
            sendOpus(opus, opus_sz, sender);
        }
    }
}

void OpusCodec::sendOpus(uint8_t* buf, int sz, int sender){
    tlv_t* tlv_channel = reinterpret_cast<tlv_t*>(opusBuf);
    tlv_channel->type = TYPE_SENDER;
    tlv_channel->len = 1;
    *reinterpret_cast<uint8_t*>(tlv_channel->value) = sender;
    
    tlv_t* tlv_opus = reinterpret_cast<tlv_t*>(opusBuf+4);
    tlv_opus->type = TYPE_OPUS;
    tlv_opus->len = sz;
    memcpy(tlv_opus->value, buf, sz);
    
    int pack_sz = TLV_SZ(tlv_channel) + TLV_SZ(tlv_opus);
    
    list<shared_ptr<Client>> tmp_clients;
    getClients(tmp_clients);
    for(auto cli : tmp_clients){
        if(!cli->active)
            continue;
        if(send(cli->sock, opusBuf, pack_sz, 0) == -1){
            cerr << "Error codec send: " << strerror(errno) << endl;
            cli->dispose();
        }
//            if(send(cli->sock, tlv_opus, TLV_SZ(tlv_opus), 0) == -1){
//                cerr << "Error codec send: " << strerror(errno) << endl;
//                cli->dispose();
//            }
    }
}
